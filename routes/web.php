<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');

    Route::resource('users', 'UsersController');
    Route::resource('users-api', 'UsersApiController');
    Route::resource('profile', 'ProfileController');
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::post('oauth/access_token', function () {
    return Response::json(Authorizer::issueAccessToken());
});

// Set password for new users
Route::get('api/get-ready/{id}', 'ApiService@getReady');
// Authenticate in app
Route::post('api/login', 'ApiService@login');
// Get authenticated user
Route::get('api/get-user', 'ApiService@getUser');
// Get api routes
Route::get('api/get-routes', 'ApiService@getRoutes');
// Get users
Route::get('api/get-users', 'ApiService@getUsers');
// Get users
Route::get('api/get-users-api', 'ApiService@getUsersAPI');
// Get users
Route::get('api/get-unused-users', 'ApiService@getUnusedUsers');

/*
|--------------------------------------------------------------------------
| Custom API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "oauth" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'oauth'], function () {
    /* Password Service */
    Route::get('api/passwords', 'PasswordService@passwords');
    Route::get('api/shared-passwords', 'PasswordService@sharedPasswords');
    Route::post('api/passwords/store', 'PasswordService@store');
    Route::put('api/passwords/update/{id}', 'PasswordService@update');
    Route::delete('api/passwords/destroy/{id}', 'PasswordService@destroy');
    /* Password Share */
    Route::get('api/users-api/{id}', 'UserAPIService@users');
    Route::put('api/share-password/{id}', 'UserAPIService@sharePassword');
    Route::post('api/remove-user/{id}', 'UserAPIService@removeUser');
});
