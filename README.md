# vue-api

Laravel OAUTH2 w/ Vue.js views (API)

## Compile

npm run watch

## Docs

Auth

- https://github.com/lucadegasperi/oauth2-server-laravel
- https://medium.com/@mshanak/laravel-5-token-based-authentication-ae258c12cfea

App

- https://laravel-news.com/using-vue-router-laravel/

Style Guide

- https://vuetifyjs.com/en/