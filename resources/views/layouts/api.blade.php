<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Password Manager API') }}</title>

    <!-- Font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">

    <!-- Theme -->
    <link href="{{ asset('/public/css/theme.css') }}" rel="stylesheet">

    <!-- Page CSS -->
    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    </script>
</head>
<body>
<div id="app">
    <app></app>
</div>

<!-- Vuetify -->
<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>

<!-- Page Scripts -->
<script src="{{ asset('/public/js/app.js') }}"></script>

@yield('js')

</body>
</html>