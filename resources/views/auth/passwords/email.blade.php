@extends('layouts.auth')

@section('content')
    <div class="container">
        <section class="blur-container">
            <div class="blur"></div>
            <div class="panel panel-default">
                <div class="panel-heading title">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group {{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                            <label for="email">E-Mail</label>

                            <input id="email" type="email" class="form-control" name="email" placeholder="E-mail"
                                   value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                @include('components.error', ['errors' => $errors->get('email')])
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="pull-right">
                                <button type="button" class="btn btn-default" onclick="window.location = '/'">
                                    Cancel
                                </button>

                                <button type="submit" class="btn btn-primary">
                                    Send Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
