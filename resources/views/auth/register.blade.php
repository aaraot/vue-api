@extends('layouts.auth')

@section('content')
    <div class="container">
        <section class="blur-container">
            <div class="blur"></div>
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error has-feedback' : '' }}">
                            <label for="name">Name</label>

                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                   placeholder="Name" required autofocus>

                            @if ($errors->has('name'))
                                @include('components.error', ['errors' => $errors->get('name')])
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                            <label for="email">E-Mail</label>

                            <input id="email" type="email" class="form-control" name="email" placeholder="E-mail"
                                   value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                @include('components.error', ['errors' => $errors->get('email')])
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error has-feedback' : '' }}">
                            <label for="password">Password</label>

                            <input id="password" type="password" class="form-control" name="password"
                                   placeholder="Password"
                                   required>

                            @if ($errors->has('password'))
                                @include('components.error', ['errors' => $errors->get('password')])
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Confirm Password</label>

                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation"
                                   placeholder="Confirm password" required>
                        </div>

                        <div class="form-group">
                            <div class="pull-right">
                                <button type="button" class="btn btn-default" onclick="window.location = '/'">
                                    Cancel
                                </button>

                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
