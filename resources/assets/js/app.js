/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Import dependencies
 */
// Router
import VueRouter from 'vue-router';
// UI Framework
import Vuetify from 'vuetify';
// Plugins
import Toasted from 'vue-toasted';
// Layout & Pages
import App from './views/App';
import Overview from './views/Overview';
import Profile from './views/Profile';
import Users from './views/Users';
import UsersAPI from './views/UsersAPI';

/**
 * Register resources
 */
Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(Toasted);

/**
 * Register routes
 */
const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Overview
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/users-api',
            name: 'users-api',
            component: UsersAPI,
        }
    ],
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('form-error', require('./components/FormError.vue'));

const app = new Vue({
    el: '#app',
    components: {App},
    router
});