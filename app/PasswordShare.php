<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordShare extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'password_shares';
}
