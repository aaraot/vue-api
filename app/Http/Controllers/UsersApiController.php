<?php

namespace App\Http\Controllers;

use App\Mail\ApiRegistration;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class UsersApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();

        try {
            $user = User::find($request->get('id'));
            $userApi = new UserAPI();

            $userApi->id = $request->get('id');
            $userApi->secret = str_random(40);
            $userApi->name = $user->email;

            $userApi->save();

            \DB::commit();

            $userApi->id = $request->get('id');

            \Mail::to($user->email)->send(new ApiRegistration($user));
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['user' => $userApi, 'message' => 'A new api user has been created!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return UserAPI::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();

        try {
            if ($request->get('user_id') !== $id) {
                $user = User::find($request->get('user_id'));
                $userApi = UserAPI::find($id);

                $userApi->id = $user->id;
                $userApi->secret = str_random(40);
                $userApi->name = $user->email;

                $userApi->save();

                \DB::commit();

                $message = 'User api has been updated!';
            } else
                $message = 'No changes were made!';
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['user' => $userApi, 'message' => $message], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            $userApi = UserAPI::find($id);
            $userApi->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['user' => $userApi, 'message' => 'User api has been deleted!'], 200);
    }
}
