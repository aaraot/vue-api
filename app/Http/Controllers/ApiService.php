<?php

namespace App\Http\Controllers;

use App\Route;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class ApiService extends Controller
{
    /**
     * Set the user ready by configuring the password
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function getReady($id)
    {
        return view('get-ready', ['id' => $id]);
    }

    /**
     * Login api users into app
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if (\Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            $user = User::where('email', $request->get('email'))->first();
            $userApi = UserAPI::where('id', $user->id)->first();

            $credentials = [
                'grant_type' => 'password',
                'client_id' => $userApi->id,
                'client_secret' => $userApi->secret,
                'name' => $user->name,
                'username' => $request->get('email'),
                'password' => $request->get('password')
            ];

            return json_encode($credentials);
        } else {
            return response()->json(['error' => 'These credentials do not match our records.'], 403);
        }
    }

    /**
     * Get authenticated user
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function getUser()
    {
        \Auth::user()->base64ID = base64_encode(\Auth::user()->id);

        return \Auth::user();
    }

    /**
     * Get all api routes
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function getRoutes()
    {
        $routes = array();

        foreach (\Route::getRoutes() as $route) {
            if (in_array('oauth', $route->middleware())) {
                $actionName = $route->getActionName();
                $controller = explode('\\', $actionName);
                $controller = isset(explode('@', end($controller))[1]) ? explode('@', end($controller))[1] : '-';
                $action = explode('@', $actionName);
                $action = isset($action[1]) ? $action[1] : '-';

                $_middleware = '';
                $max = count($route->middleware()) - 1;
                foreach ($route->middleware() as $key => $value) {
                    $_middleware .= $value;

                    if ($key !== $max)
                        $_middleware .= ', ';
                }

                $_controllerMiddleware = '';
                $max = count($route->controllerMiddleware());
                if ($max > 0)
                    foreach ($route->controllerMiddleware() as $key => $value) {
                        $_controllerMiddleware .= $value;

                        if ($key !== ($max - 1))
                            $_controllerMiddleware .= ', ';
                    }

                array_push($routes, new Route($route->uri(), $route->methods()[0], $controller, $action, $_middleware, $_controllerMiddleware));
            }
        }

        return $routes;
    }

    /**
     * Get all users
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function getUsers()
    {
        return User::all();
    }

    /**
     * Get all users api
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function getUsersAPI()
    {
        return UserAPI::all();
    }

    /**
     * Get all unused users
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function getUnusedUsers()
    {
        return User::whereNotIn('id', UserAPI::get(['id']))->get();
    }
}
