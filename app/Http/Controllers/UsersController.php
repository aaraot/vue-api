<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => 'required|unique:users|email'
        ])->validate();

        \DB::beginTransaction();

        try {
            $user = new User();

            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->password = bcrypt('password');

            $user->save();

            $profile = new Profile();

            $profile->user_id = $user->id;

            $profile->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['user' => $user, 'message' => 'A new user has been created!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [];

        if ($request->get('emailChanged')) {
            $rules = [
                'name' => 'required|max:191',
                'email' => 'required|unique:users|email'
            ];
        } else {
            $rules = ['name' => 'required|max:191'];
        }

        \Validator::make($request->all(), $rules)->validate();

        \DB::beginTransaction();

        try {
            $user->name = $request->get('name');
            $user->email = $request->get('email');

            $user->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['user' => $user, 'message' => 'User has been updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        \DB::beginTransaction();

        try {
            $user->delete();

            $userApi = UserAPI::find($user->id);

            if ($userApi)
                $userApi->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['message' => 'User has been deleted!'], 200);
    }
}
