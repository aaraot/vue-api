<?php

namespace App\Http\Controllers;

use App\PasswordShare;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class UserAPIService extends Controller
{
    /**
     * Gets api users that doesn't have access to specified resource
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function users($id)
    {
        $ids = [\LucaDegasperi\OAuth2Server\Facades\Authorizer::getResourceOwnerId()];

        foreach (PasswordShare::where('password_id', $id)->get() as $share) {
            array_push($ids, $share->user);
        }

        $users = UserAPI::whereNotIn('id', $ids)->get();
        unset($ids[0]);
        $shared = User::whereIn('id', $ids)->get();

        return json_encode(['users' => $users, 'shared' => $shared]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function sharePassword(Request $request, $id)
    {
        \DB::beginTransaction();

        try {
            foreach ($request->get('users') as $value) {
                $share = new PasswordShare();

                $share->password_id = $id;
                $share->user = $value;

                $share->save();
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => 'Oops! Something went wrong, please try again!'], 422);
        }

        return response()->json(['message' => 'The password has been updated!'], 200);
    }

    /**
     * Remove the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function removeUser(Request $request, $id)
    {
        \DB::beginTransaction();

        try {
            PasswordShare::where('password_id', $id)->where('user', $request->get('user_id'))->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['message' => 'The user has been removed!', 'shared' => UserAPI::find($request->get('user_id'))], 200);
    }
}
