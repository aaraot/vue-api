<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->has('password') && $request->get('password') !== '') {
            \Validator::make($request->all(), [
                'name' => 'required|min:6|max:191'
            ])->validate();
        } else {
            \Validator::make($request->all(), [
                'password' => 'required|min:6',
                'confirmPassword' => 'required|min:6|same:password'
            ])->validate();
        }

        \DB::beginTransaction();

        try {
            $user = User::find(base64_decode($id));

            if (!$request->has('password')) {
                $user->name = $request->get('name');
            } else {
                $user->password = bcrypt($request->get('password'));
            }

            $user->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['message' => 'Your profile has been updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
