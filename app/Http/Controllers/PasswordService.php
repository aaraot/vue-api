<?php

namespace App\Http\Controllers;

use App\Password;
use App\PasswordShare;
use Illuminate\Http\Request;

class PasswordService extends Controller
{
    /**
     * Gets all the passwords for the logged user
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function passwords()
    {
        return json_encode(Password::whereIn('user', [\LucaDegasperi\OAuth2Server\Facades\Authorizer::getResourceOwnerId()])->get());
    }

    /**
     * Gets all the shared passwords for the logged user
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function sharedPasswords()
    {
        $sharedPasswords = PasswordShare::whereIn('user', [\LucaDegasperi\OAuth2Server\Facades\Authorizer::getResourceOwnerId()])->get();

        $ids = [];
        foreach ($sharedPasswords as $password) {
            array_push($ids, $password->password_id);
        }

        return json_encode(Password::whereIn('id', $ids)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();

        try {
            $password = new Password();

            $password->project_name = $request->get('password')['project_name'];
            $password->project_link = $request->get('password')['project_link'];
            $password->description = $request->get('password')['description'];
            $password->username = $request->get('password')['username'];
            $password->password = $request->get('password')['password'];
            $password->user = $request->get('user');

            $password->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => 'Oops! Something went wrong, please try again!'], 422);
        }

        return response()->json(['password' => $password, 'message' => 'A new password has been created!'], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();

        try {
            $password = Password::find($id);

            $password->project_name = $request->get('project_name');
            $password->project_link = $request->get('project_link');
            $password->description = $request->get('description');
            $password->username = $request->get('username');
            $password->password = $request->get('password');
            $password->user = $request->get('user');

            $password->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => 'Oops! Something went wrong, please try again!'], 422);
        }

        return response()->json(['password' => $password, 'message' => 'The password was shared!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            $password = Password::find($id);

            $password->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['message' => 'Password has been deleted!'], 200);
    }
}
